# Changelog

## Unreleased

### Added

- Installable en tant que package Composer

### Changed

- Compatible SPIP 5.0.0-dev

### Fixed

- Coquille dans le modèle forum lors de l’introduction d’un `attribut_url`
